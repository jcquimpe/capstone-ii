	//select the HTML elements that we'll be working with
const cancelBtn = document.querySelector('#cancelBtn');

const csrfToken = document.querySelector('meta[name="csrf-token"]').getAttribute('content');

cancelBtn.addEventListener('click', ()=>{
	let data = new FormData;
	//append() method of the FormData object takes in 2 arguments:
		//what would be the property name
		//what would be the property value
	data.append('', catInput.value);

	fetch("/transactions/", {
		method: "POST",
		body: data,
		//put the CSRF token in the request header
		headers: {
			'X-CSRF-TOKEN': csrfToken
		}
	})
	//when the promise object returned by our fetch request resolves, execute the anonymous function passed in to the then() method:
	.then((res)=>{
		
		return res.json();
	})
	.then((data)=>{
		if(data.data){
			//append the content of the response object's data property to the options under the select element in the products.create view
			catSelect.innerHTML += data.data;
			catInput.value = "";
		}
		//output the response object's message property in the designated notification element
		addCatNotif.innerHTML = data.message;
	})

})