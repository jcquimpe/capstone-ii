<?php

use Illuminate\Database\Seeder;

class SeriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('series')->delete();
        
        \DB::table('series')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Nissan Maxima Platinum 2020',
                'description' => 'This isn’t just the 4-door Sports Car, it’s a high-tech powerhouse. Change from a premium ride to canyon carver at the touch of a button. Feel more confident, thanks to available advanced driver assist technologies, and for seamless connectivity, just plug in your compatible smartphone.  All so you can focus on a thrilling drive. This is tech that raises your pulse. This is tech that moves.',
                'img_path' => 'images/1587711859.jfif',
                'isActive' => 1,
                'category_id' => 1,
                'created_at' => '2020-04-24 07:04:19',
                'updated_at' => '2020-04-27 10:18:35',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Porsche Carrera GT',
            'description' => 'The Porsche Carrera GT (Project Code 980) is a mid-engine sports car that was manufactured by German automobile manufacturer Porsche between 2004–2007. Sports Car International named the Carrera GT number one on its list of Top Sports Cars of the 2000s, and number eight on Top Sports Cars of All Time list. For its advanced technology and development of its chassis, Popular Science magazine awarded it the &quot;Best of What\'s New&quot; award in 2003.',
                'img_path' => 'images/1587711923.jfif',
                'isActive' => 1,
                'category_id' => 2,
                'created_at' => '2020-04-24 07:05:23',
                'updated_at' => '2020-04-24 10:05:27',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Honda CR-V 2020',
                'description' => 'The 2020 Honda CR-V ranks among the leaders of the compact SUV class. Its passenger and cargo volumes eclipse those of most rivals, and its well-balanced ride and capable powertrain make it a great daily driver.',
                'img_path' => 'images/1587712095.jpg',
                'isActive' => 1,
                'category_id' => 3,
                'created_at' => '2020-04-24 07:08:15',
                'updated_at' => '2020-04-24 07:08:15',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => '2018 Ford Focus ST-Line X',
                'description' => 'The Ford Focus is manufactured by the Ford Motor Company and created under Alexander Trotman\'s Ford 2000 plan, which aimed to globalize model development and sell one compact vehicle worldwide. The original Focus was primarily designed by Ford of Europe\'s German and British teams.',
                'img_path' => 'images/1587712334.jpg',
                'isActive' => 1,
                'category_id' => 4,
                'created_at' => '2020-04-24 07:12:14',
                'updated_at' => '2020-04-24 14:40:36',
            ),
        ));
        
        
    }
}