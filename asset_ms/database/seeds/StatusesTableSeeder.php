<?php

use Illuminate\Database\Seeder;

class StatusesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('statuses')->delete();
        
        \DB::table('statuses')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'PENDING',
                'created_at' => '2020-04-27 00:00:00',
                'updated_at' => '2020-04-27 00:00:00',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'APPROVED',
                'created_at' => '2020-04-27 00:00:00',
                'updated_at' => '2020-04-27 00:00:00',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'REJECTED',
                'created_at' => '2020-04-27 00:00:00',
                'updated_at' => '2020-04-27 00:00:00',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'CANCELLED',
                'created_at' => '2020-04-27 00:00:00',
                'updated_at' => '2020-04-27 00:00:00',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'RETURNED',
                'created_at' => '2020-04-27 00:00:00',
                'updated_at' => '2020-04-27 00:00:00',
            ),
        ));
        
        
    }
}