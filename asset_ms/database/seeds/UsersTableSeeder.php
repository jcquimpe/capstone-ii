<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'admin',
                'email' => 'admin@mail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$VM2E.7xPi59WDyloHvUvC.EUHCKjruicdSYRDbECis6tXpXm21ZQO',
                'remember_token' => NULL,
                'created_at' => '2020-04-24 06:57:16',
                'updated_at' => '2020-04-24 06:57:16',
                'role_id' => 1,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'John',
                'email' => 'john@mail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$NAsLPTG7BmgKhFarzwOWQOivM6QbW7b3ggtu1McK4qaepEus2VbFy',
                'remember_token' => NULL,
                'created_at' => '2020-04-24 06:57:50',
                'updated_at' => '2020-04-24 06:57:50',
                'role_id' => 2,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'mae',
                'email' => 'mae@mail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$rkq6GZs2ioyJqAf31M36JOJutSS3fOvAOzxlRc9MpAd.cMSxuaa0m',
                'remember_token' => NULL,
                'created_at' => '2020-04-28 10:58:19',
                'updated_at' => '2020-04-28 10:58:19',
                'role_id' => 2,
            ),
        ));
        
        
    }
}