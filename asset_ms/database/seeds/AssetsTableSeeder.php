<?php

use Illuminate\Database\Seeder;

class AssetsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('Assets')->delete();
        
        \DB::table('Assets')->insert(array (
            0 => 
            array (
                'id' => 1,
                'modelNo' => 'sdn-1664837049877627-nmp',
                'isAvailable' => 1,
                'series_id' => 1,
                'created_at' => '2020-04-24 07:12:37',
                'updated_at' => '2020-04-27 10:12:14',
            ),
            1 => 
            array (
                'id' => 2,
                'modelNo' => 'spc-1664837049877627-pcg',
                'isAvailable' => 1,
                'series_id' => 2,
                'created_at' => '2020-04-24 07:13:10',
                'updated_at' => '2020-04-24 07:13:10',
            ),
            2 => 
            array (
                'id' => 3,
                'modelNo' => 'suv-1664837049877627-hcv',
                'isAvailable' => 1,
                'series_id' => 3,
                'created_at' => '2020-04-24 07:13:25',
                'updated_at' => '2020-04-24 07:13:25',
            ),
            3 => 
            array (
                'id' => 4,
                'modelNo' => 'hbk-1664837049877627-ffslx',
                'isAvailable' => 1,
                'series_id' => 4,
                'created_at' => '2020-04-24 07:13:41',
                'updated_at' => '2020-04-24 07:13:41',
            ),
            4 => 
            array (
                'id' => 5,
                'modelNo' => 'sdn-1665031194996564-nmp',
                'isAvailable' => 1,
                'series_id' => 1,
                'created_at' => '2020-04-26 11:09:58',
                'updated_at' => '2020-04-26 11:09:58',
            ),
            5 => 
            array (
                'id' => 6,
                'modelNo' => 'spc-1665481953538695-pcg',
                'isAvailable' => 1,
                'series_id' => 2,
                'created_at' => '2020-05-01 10:02:57',
                'updated_at' => '2020-05-01 10:02:57',
            ),
            6 => 
            array (
                'id' => 7,
                'modelNo' => 'csuv-1665481980342257-hcv',
                'isAvailable' => 1,
                'series_id' => 3,
                'created_at' => '2020-05-01 10:03:25',
                'updated_at' => '2020-05-01 10:03:25',
            ),
            7 => 
            array (
                'id' => 8,
                'modelNo' => 'hbk-1665482002580219-ffslx',
                'isAvailable' => 1,
                'series_id' => 4,
                'created_at' => '2020-05-01 10:03:50',
                'updated_at' => '2020-05-01 10:03:50',
            ),
        ));
        
        
    }
}