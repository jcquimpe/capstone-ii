<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('categories')->delete();
        
        \DB::table('categories')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Sedan',
                'model_code' => 'sdn',
                'created_at' => '2020-04-24 07:00:27',
                'updated_at' => '2020-04-24 07:00:27',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Supercar',
                'model_code' => 'spc',
                'created_at' => '2020-04-24 07:00:35',
                'updated_at' => '2020-04-24 07:00:35',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'SUV',
                'model_code' => 'csuv',
                'created_at' => '2020-04-24 07:00:41',
                'updated_at' => '2020-04-24 07:00:41',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Hatchback',
                'model_code' => 'hbk',
                'created_at' => '2020-04-24 07:00:56',
                'updated_at' => '2020-04-24 07:00:56',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Compact',
                'model_code' => 'cpt',
                'created_at' => '2020-04-24 07:01:04',
                'updated_at' => '2020-04-24 07:01:04',
            ),
        ));
        
        
    }
}