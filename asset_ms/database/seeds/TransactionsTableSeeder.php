<?php

use Illuminate\Database\Seeder;

class TransactionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('transactions')->delete();
        
        \DB::table('transactions')->insert(array (
            0 => 
            array (
                'id' => 1,
                'refNo' => '1665470504923414',
                'user_id' => 2,
                'status_id' => 1,
                'series_id' => 1,
                'asset_id' => NULL,
                'borrow_date' => '2020-05-02 00:00:00',
                'return_date' => '2020-05-03 00:00:00',
                'created_at' => '2020-05-01 07:00:45',
                'updated_at' => '2020-05-01 07:00:45',
            ),
            1 => 
            array (
                'id' => 2,
                'refNo' => '1665470520738811',
                'user_id' => 2,
                'status_id' => 1,
                'series_id' => 1,
                'asset_id' => NULL,
                'borrow_date' => '2020-05-04 00:00:00',
                'return_date' => '2020-05-05 00:00:00',
                'created_at' => '2020-05-01 07:01:00',
                'updated_at' => '2020-05-01 07:01:00',
            ),
            2 => 
            array (
                'id' => 3,
                'refNo' => '1665470810437175',
                'user_id' => 3,
                'status_id' => 1,
                'series_id' => 2,
                'asset_id' => NULL,
                'borrow_date' => '2020-05-14 00:00:00',
                'return_date' => '2020-05-15 00:00:00',
                'created_at' => '2020-05-01 07:05:36',
                'updated_at' => '2020-05-01 07:05:36',
            ),
            3 => 
            array (
                'id' => 4,
                'refNo' => '1665470823886534',
                'user_id' => 3,
                'status_id' => 1,
                'series_id' => 3,
                'asset_id' => NULL,
                'borrow_date' => '2020-05-25 00:00:00',
                'return_date' => '2020-05-26 00:00:00',
                'created_at' => '2020-05-01 07:05:49',
                'updated_at' => '2020-05-01 07:05:49',
            ),
            4 => 
            array (
                'id' => 5,
                'refNo' => '1665470840321074',
                'user_id' => 3,
                'status_id' => 1,
                'series_id' => 4,
                'asset_id' => NULL,
                'borrow_date' => '2020-05-12 00:00:00',
                'return_date' => '2020-05-13 00:00:00',
                'created_at' => '2020-05-01 07:06:05',
                'updated_at' => '2020-05-01 07:06:05',
            ),
            5 => 
            array (
                'id' => 6,
                'refNo' => '1665470848680898',
                'user_id' => 3,
                'status_id' => 1,
                'series_id' => 1,
                'asset_id' => NULL,
                'borrow_date' => '2020-05-06 00:00:00',
                'return_date' => '2020-05-07 00:00:00',
                'created_at' => '2020-05-01 07:06:13',
                'updated_at' => '2020-05-01 07:06:13',
            ),
            6 => 
            array (
                'id' => 7,
                'refNo' => '1665470902902068',
                'user_id' => 2,
                'status_id' => 1,
                'series_id' => 2,
                'asset_id' => NULL,
                'borrow_date' => '2020-05-15 00:00:00',
                'return_date' => '2020-05-16 00:00:00',
                'created_at' => '2020-05-01 07:07:04',
                'updated_at' => '2020-05-01 07:07:04',
            ),
            7 => 
            array (
                'id' => 8,
                'refNo' => '1665470912935539',
                'user_id' => 2,
                'status_id' => 1,
                'series_id' => 3,
                'asset_id' => NULL,
                'borrow_date' => '2020-05-22 00:00:00',
                'return_date' => '2020-05-23 00:00:00',
                'created_at' => '2020-05-01 07:07:14',
                'updated_at' => '2020-05-01 07:07:14',
            ),
            8 => 
            array (
                'id' => 9,
                'refNo' => '1665470924287539',
                'user_id' => 2,
                'status_id' => 1,
                'series_id' => 4,
                'asset_id' => NULL,
                'borrow_date' => '2020-05-10 00:00:00',
                'return_date' => '2020-05-11 00:00:00',
                'created_at' => '2020-05-01 07:07:25',
                'updated_at' => '2020-05-01 07:07:25',
            ),
            9 => 
            array (
                'id' => 10,
                'refNo' => '1665470986846107',
                'user_id' => 2,
                'status_id' => 1,
                'series_id' => 4,
                'asset_id' => NULL,
                'borrow_date' => '2020-05-30 00:00:00',
                'return_date' => '2020-05-31 00:00:00',
                'created_at' => '2020-05-01 07:08:24',
                'updated_at' => '2020-05-01 07:08:24',
            ),
        ));
        
        
    }
}