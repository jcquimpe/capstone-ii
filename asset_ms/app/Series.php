<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Series extends Model
{

    public function category()
    {
    	return $this->belongsTo('\App\Category');
    }

	public function transactions()
	{
		return $this->hasMany('\App\Transaction');
	}

	public function assets()
	{
		return $this->hasMany('\App\Asset');
	}
}
