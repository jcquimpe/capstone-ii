<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function series()
    {
    	return $this->hasMany('\App\Series');
    }
}
