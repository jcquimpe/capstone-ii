<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Asset extends Model
{
    public function series()
    {
    	return $this->belongsTo('\App\Series');
    }
    public function transactions()
    {
    	return $this->hasMany('\App\Transaction');
    }
}
