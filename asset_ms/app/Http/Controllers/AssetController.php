<?php

namespace App\Http\Controllers;

use App\Asset;
use App\Category;
use App\Series;
use Auth;

use Illuminate\Http\Request;

class AssetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->role_id === 1);
        $assets= Asset::all();
        return view('assets.index')->with('assets', $assets);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         //use the authorize() controller helper to check he create() action of the ProductPolicy class
        //if authorization fails the check, we expect an HTTP response of status code 403
        $this->authorize('create', Series::class);
        $asset= Asset::all();
        $categories = Category::all();
        $items = Series::all();

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->input('series'));
        $this->authorize('create', Series::class);

        $request->validate([
            'series' => 'required|string|unique:series,name',
            'modelNo' => 'string',
        ]);

        $series = htmlspecialchars($request->input('series'));
        
        $modelNo = htmlspecialchars($request->input('modelNo'));

 
        $asset = new Asset;
        //set the properties of this object as defined in its migration to their respective sanitized form input values
        

        $asset->series_id = $series;
        $asset->modelNo = $modelNo;
        $asset->save();

        return redirect('/series');    
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function show(Asset $asset)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function edit(Asset $asset)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Asset $asset)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function destroy(Asset $asset)
    {
        
        if($asset->isAvailable == 1){
            $asset->isAvailable = 0;
        }
        else{
            $asset->isAvailable = 1;
        }
        $asset->save();

        return redirect('/series/'.$asset->series->id);
    }
}
