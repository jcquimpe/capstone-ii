<?php

namespace App\Http\Controllers;

use App\Series;
use App\Asset;
use App\Category;
use App\Transaction;
use Illuminate\Http\Request;
use Auth;

class SeriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $cars = Series::all();
        $assets = Asset::all();
        
        //dd($asset->series->category_id);
        $categories = Category::all();
        return view('series.index')->with('categories', $categories)->with('cars', $cars)->with('assets', $assets);


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', Series::class);
        $categories = Category::all();
        $items = Series::all();
        return view('series.create')->with('categories', $categories)->with('items', $items);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', Series::class);

        $request->validate([
            'name' => 'required|string|unique:series,name',
            'description' => 'required|string',
            'category' => 'required',
            'image' =>'required|image'
        ]);

        $name = htmlspecialchars($request->input('name'));
        
        $description = htmlspecialchars($request->input('description'));
        $category = htmlspecialchars($request->input('category'));
        $image = $request->file('image');

        $item = new Series;

        $item->name = $name;
        $item->description = $description;
        $item->category_id = $category;

        $file_name = time(). "." . $image->getClientOriginalExtension();

        $destination = "images/";

        $image->move($destination, $file_name);
        //set the path of the saved image as the value for the column img_path of this record
        $item->img_path = $destination.$file_name;

        //save the new product object as a new record in the products table via its save() method
        $item->save();
        //this will now call the index action of the ProductController as per laravel's resourceful routes
        return redirect('/series/create');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Series  $series
     * @return \Illuminate\Http\Response
     */
    public function show(Series $series)
    {
        if(Auth::user()->role_id === 1);
        $categories = Category::all();
        $assets = Asset::where('series_id', $series->id)->get();
        
        return view('series.show')->with('categories', $categories)->with('series', $series)->with('assets', $assets);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Series  $series
     * @return \Illuminate\Http\Response
     */
    public function edit(Series $series)
    {

        $categories = Category::all();
        return view('series.edit')->with('series', $series)->with('categories', $categories);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Series  $series
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Series $series)
    {
        $this->authorize('update', Series::class);
        
        $request->validate([
            'name' => 'required|string',
            'description' => 'required|string',
            'category' => 'required',
            'image' =>'image'
        ]);

        $name = htmlspecialchars($request->input('name'));
        
        $description = htmlspecialchars($request->input('description'));
        $category = htmlspecialchars($request->input('category'));

        //$product is the product object to be edited, this was obtained via laravel's route-model binding
        //overwrite the properties of $product with the input values from the edit form
        $series->name = $name;
        $series->description = $description;
        $series->category_id = $category;

        //if an image file upload is found, replace the current image of the product with the new upload
        if($request->file('image') != null){
            $image = $request->file('image');
            $file_name = time(). "." . $image->getClientOriginalExtension();
              //set target destination where the file will be saved in 
            $destination = "images/";
            //call the move() method of the $image object to save the uploaded file in the target destination under the specified file name
            $image->move($destination, $file_name);
            //set the path of the saved image as the value for the column img_path of this record
            $series->img_path = $destination.$file_name;

        }
        //save the new product object as a new record in the products table via its save() method
        $series->save();
        //this will now call the index action of the ProductController as per laravel's resourceful routes
        return redirect("/series/".$series->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Series  $series
     * @return \Illuminate\Http\Response
     */
    public function destroy(Series $series)
    {

        if($series->isActive == 1){
            $series->isActive = 0;
        }
        else{
            $series->isActive = 1;
        }

        $series->save();

        return redirect('/series/');
    }


}
