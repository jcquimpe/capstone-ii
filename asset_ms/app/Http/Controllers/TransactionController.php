<?php

namespace App\Http\Controllers;

use App\Transaction;
use App\Series;
use App\Asset;
use Illuminate\Http\Request;
use Auth;
use Carbon\Carbon;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $assets = Asset::all();
        $series = Series::all();
        if (Auth::user()->role_id === 1) {
            $transactions = Transaction::all();
        }
        else{
            $transactions = Transaction::where('user_id', Auth::user()->id)->get();
        }

        return view('transactions.index')->with('series', $series)->with('assets', $assets)->with('transactions', $transactions);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'borrow_date' => 'required|date',
            'return_date' => 'required|date'
        ]);

        $borrow_date = htmlspecialchars($request->input('borrow_date'));
        $return_date = htmlspecialchars($request->input('return_date'));

        $transaction = new Transaction;

        $request->id = Auth::user()->id;


        $transaction->refNo = hexdec(uniqid());
        $transaction->user_id = $request->id;
        $transaction->status_id = 1;
        $transaction->series_id = $request->input('series_id');
        $transaction->asset_id = null;
        $transaction->borrow_date = $request->input('borrow_date');
        $transaction->return_date = $request->input('return_date');
        $transaction->save();

        if($transaction->save()){
        $request->session()->flash('status', "Your request has been submitted.");
        }
        return redirect('/series')->with('transaction', $transaction);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function show(Transaction $transaction)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function edit(Transaction $transaction)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transaction $transaction)
    {
        
        if (Auth::user()->role_id === 1) {
            if($transaction->status_id == 1){
                $car = $transaction->series_id;
                $newAsset = $transaction->series->assets->where('isAvailable', 1)->first();
                if($newAsset == null){
                    $request->session()->flash('status', "There's no available asset as of now.");
                }
                else{

                    $transaction->asset_id = $newAsset->id;
                    if($transaction->asset->isAvailable == 1){
                        $transaction->asset->isAvailable = 0;
                        $transaction->asset->save();
                    }
                    $transaction->status_id = 2;
                    $transaction->save();


                }
                return redirect('/transactions');
            }

            else if($transaction->status_id == 2){
                $transaction->status_id = 5;
                $transaction->asset->isAvailable = 1;
                $transaction->asset->save();
                $transaction->save();
                return redirect('/transactions');
            }

            else{
                $request->session()->flash('status', "There's no available asset as of now.");
            }
        }
        else if(Auth::user()->role_id === 2)
        {
            if($transaction->status_id == 1){
                $transaction->status_id = 4;

                $transaction->refNo = $transaction->series->category->model_code."-".$transaction->refNo;
                $transaction->save();
                return redirect('/transactions');
            }
        }
        else{
            $request->session()->flash('status', "You are not authorized to access this page! Please log-in.");
            return view('/login');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transaction $transaction)
    {
        if (Auth::user()->role_id === 1) {
            if($transaction->status_id == 1){
                $transaction->status_id = 3;

                $transaction->refNo = $transaction->series->category->model_code."-".$transaction->refNo;
                $transaction->save();
                return redirect('/transactions');
            }
        }
    }
}
