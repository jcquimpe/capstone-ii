<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    public function user()
    {
    	return $this->belongsTo('\App\User');
    }

    public function status()
    {
    	return $this->belongsTo('\App\Status');
    }

    public function series()
    {
    	return $this->belongsTo('\App\Series');
    }

    public function asset()
    {
    	return $this->belongsTo('\App\Asset');
    }
}
