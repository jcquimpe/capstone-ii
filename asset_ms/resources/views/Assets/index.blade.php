@extends('layouts.app')

@section('content')
		<div class="row">
		<div class="col-8 offset-2">
			@if ($errors->any())
			    <div class="alert alert-danger">
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
			@endif
			{{-- call the Gate defined in AuthServiceProvider --}}
			@can('isAdmin')
				{{-- admin dashboard view --}}
				<h2>Assets</h2>
				<table class="table table-dark">
					<thead>
						<tr>
							<th>Name:</th>
							<th>Series:</th>
							<th>Status:</th>
							<th>Category:</th>
							<th>Actions:</th>
						</tr>
					</thead>
					<tbody>
						@foreach($assets as $asset)
						    <tr>
						    	<td style="width: 10%;"><img class="img-fluid" src="{{$asset->series->img_path}}"></td>
							    <td><a href="/series/{{$asset->series->id}}">{{$asset->modelNo}}</a></td>
							    <td>
							      	@if($asset->series->isActive == 1)
							      		{{"Available"}}
							      	@else
							      		{{"Unavailable"}}
							      	@endif
							    </td>
							    <td>{{$asset->series->category->name}}</td>
							    <td>
							    	<a href="/series/{{$asset->series->id}}/edit" class="btn btn-warning mb-1">Edit</a>
						      		<form method="POST" action="/series/{{$asset->series->id}}">
						      			@csrf
						      			@method('DELETE')
						      			@if($asset->series->isActive == 1)
						      				<button type="submit" class="btn btn-danger">Deactivate</button>
						      			@else
						      				<button type="submit" class="btn btn-success">Reactivate</button>
						      			@endif
						      		</form>
						     	</td>
						    </tr>
					    @endforeach
					</tbody>
				</table>
			@else
				<div class="jumbotron">
					<h1>You're not supposed to be here.</h1>
					<p>Why don't you go back to the Catalogue View?</p>
					<a class="btn btn-primary btn-lg" href="/series">Catalogue View</a>
				</div>
			@endcan
		</div>
	</div>
@endsection