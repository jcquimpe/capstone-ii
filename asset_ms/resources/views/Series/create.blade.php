@extends('layouts.app')

@section('content')
	<div class="row">
	  	<div class="col-3">
		    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
			    <a class="nav-link" id="v-pills-dashboard-tab" data-toggle="pill" href="#v-pills-dashboard" role="tab" aria-controls="v-pills-dashboard" aria-selected="true">Dashboard</a>
			    <a class="nav-link active" id="v-pills-addCategory-tab" data-toggle="pill" href="#v-pills-addCategory" role="tab" aria-controls="v-pills-addCategory" aria-selected="false">Add Category</a>
			    <a class="nav-link" id="v-pills-series-tab" data-toggle="pill" href="#v-pills-series" role="tab" aria-controls="v-pills-series" aria-selected="false">Add Series</a>
			    <a class="nav-link" id="v-pills-asset-tab" data-toggle="pill" href="#v-pills-asset" role="tab" aria-controls="v-pills-asset" aria-selected="false">Add New Asset</a>
		    </div>
  		</div>
		<div class="col-9">
		    <div class="tab-content" id="v-pills-tabContent">
		      	<div class="tab-pane fade" id="v-pills-dashboard" role="tabpanel" aria-labelledby="v-pills-dashboard-tab">
		      		<div class="alert alert-primary">
						<h3>Go to <a class="btn btn-outline-primary" href="/series">Admin Dashboard</a></h3>
					</div>
		      	</div>
		      	
		      	<div class="tab-pane fade show active" id="v-pills-addCategory" role="tabpanel" aria-labelledby="v-pills-addCategory-tab">
		      	{{-- start of Add Category --}}
		      		<div class="card">
		      			@if ($errors->any())
						    <div class="alert alert-danger">
						        <ul>
						            @foreach ($errors->all() as $error)
						                <li>{{ $error }}</li>
						            @endforeach
						        </ul>
						    </div>
						@endif
						<div class="card-body">
							
							<div id="addCatNotif"></div>
							<form method="POST" action="/categories">
								@csrf
								<div class="form-group">
									<label for="name">Category name:</label>
									<input class="form-control" type="text" id="catName" name="catName">

									<label for="model_code">Category code:</label>
									<input class="form-control" type="text" id="catCode" name="catCode">
								</div>
							<button type="submit" class="btn btn-primary" id="addCatBtn">Add Category</button>
							</form>
						</div>
		      		</div>
		      	{{-- end of add category --}}
		      	</div>

		      	<div class="tab-pane fade" id="v-pills-series" role="tabpanel" aria-labelledby="v-pills-series-tab">
		      		{{-- start of add series card --}}
					<div class="card">
						@if ($errors->any())
						    <div class="alert alert-danger">
						        <ul>
						            @foreach ($errors->all() as $error)
						                <li>{{ $error }}</li>
						            @endforeach
						        </ul>
						    </div>
						@endif
						<div class="card-body">
							<form method="POST" action="/series" enctype="multipart/form-data">
								@csrf
								<div class="form-group">
									<label for="name">Name: </label>
									<input class="form-control" type="text" name="name" id="name">
								</div>
								<div class="form-group">
									<label for="description">Description: </label>
									<input class="form-control" type="text" name="description" id="description">
								</div>


								<div class="form-group">
									<label for="category_id">Category: </label>
									<select class="form-control" id="category_id" name="category">
										<option>Select a category:</option>
										@if(count($categories)>0)
											@foreach($categories as $category)
												<option value="{{$category->id}}">{{$category->name}}</option>
											@endforeach
										@endif
									</select>
								</div>
								<div class="form-group">
									<label for="image">Image:</label>
									<input class="form-control" type="file" name="image" id="image">
								</div>
								<button type="submit" class="btn btn-primary">Add Series</button>
							</form>
						</div>
					</div>
					{{-- end of add series card--}}
		      	</div>

		      	<div class="tab-pane fade" id="v-pills-asset" role="tabpanel" aria-labelledby="v-pills-asset-tab">
		      		{{-- start of add new asset --}}
		      		<div class="card">
						@if ($errors->any())
						    <div class="alert alert-danger">
						        <ul>
						            @foreach ($errors->all() as $error)
						                <li>{{ $error }}</li>
						            @endforeach
						        </ul>
						    </div>
						@endif
						<div class="card-body">
							<form method="POST" action="/assets">
								@csrf
								<div class="form-group">
									<label for="series_id">Series:</label>
									<select class="form-control" id="series_id" name="series">
										<option>Select a series:</option>
										@if(count($items)>0)
											@foreach($items as $item)
												<option value="{{$item->id}}">{{$item->name}}</option>
											@endforeach
										@endif
									</select>
								</div>
								<div class="form-group">
									<label for="modelNo">Model No:</label>
									<input class="form-control" type="text" id="modelNo" name="modelNo" value="{{$category->model_code."-".hexdec(uniqid())}}">
								</div>

								<div class="form-group">	
									<label for="quantity">Quantity:</label>
									<input class="form-control" type="number" id="quantity" name="quantity" min="1">
								</div>

								<button type="submit" class="btn btn-primary">Add Asset</button>
							</form>
						</div>
					</div>
					{{-- end of add new asset --}}
		      	</div>
		    </div>
		</div>
	</div>
@endsection
