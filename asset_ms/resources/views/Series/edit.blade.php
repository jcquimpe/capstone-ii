@extends('layouts.app')

@section('content')
	<div class="row">
		<div class="col-8 offset-2">
			@if ($errors->any())
			    <div class="alert alert-danger">
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
			@endif
			{{-- start of add series card --}}
			<div class="card">
				<div class="card-header">
					Edit Series
				</div>

				<div class="card-body">
					<form method="POST" action="/series/{{$series->id}}" enctype="multipart/form-data">
						@csrf
						@method('PUT')
						<div class="form-group">
								<label for="name">Name: </label>
								<input class="form-control" type="text" name="name" id="name" value="{{$series->name}}">
							</div>
							<div class="form-group">
								<label for="description">Description: </label>
								<input class="form-control" type="text" name="description" id="description" value="{{$series->description}}">
							</div>
							<div class="form-group">
								<label for="category_id">Category: </label>
								<select class="form-control" id="category_id" name="category">
									<option>Select a category:</option>
									@if(count($categories)>0)
										@foreach($categories as $category)
											@if($series->category_id == $category->id)
												<option value="{{$category->id}}" selected>{{$category->name}}</option>
											@else
												<option value="{{$category->id}}">{{$category->name}}</option>
											@endif
										@endforeach
									@endif
								</select>
							</div>
							<div class="form-group">
								<label for="image">Image:</label>
								<input class="form-control" type="file" name="image" id="image">
							</div>
							<button type="submit" class="btn btn-success">Edit Series</button>
					</form>
				</div>
			</div>
			{{-- end of add series card--}}
		</div>
	</div>
@endsection