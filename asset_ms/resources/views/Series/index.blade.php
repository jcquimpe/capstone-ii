@extends('layouts.app')

@section('content')
		<div class="row">
		<div class="col-8 offset-2">
			
			{{-- call the Gate defined in AuthServiceProvider --}}
			@can('isAdmin')
				{{-- admin dashboard view --}}
				<h2>Series</h2>
				<table class="table table-dark">
					<thead>
						<tr>
							<th></th>
							<th>Series</th>
							<th>Status</th>
							<th>Category</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
						@foreach($cars as $car)
							<tr>
						    	<td style="width: 10%;"><img class="img-fluid" src="{{$car->img_path}}"></td>
							    <td><a href="/series/{{$car->id}}">{{$car->name}}</a></td>
							    <td>
							      	@if($car->isActive == 1)
							      		{{"Active"}}
							      	@else
							      		{{"Inactive"}}
							      	@endif
							    </td>
							    <td>{{$car->category->name}}</td>
							    <td>
							    	<form method="GET" action="/series/{{$car->id}}">
							    		<button type="submit" class=" btn btn-outline-info mb-1">View Assets</button>
							    	</form>
							    	<a href="/series/{{$car->id}}/edit" class="btn btn-outline-warning mb-1 px-4">Edit</a>
						      		<form method="POST" action="/series/{{$car->id}}">
						      			@csrf
						      			@method('DELETE')
						      			@if($car->isActive == 1)
						      				<button type="submit" class="btn btn-danger">Deactivate</button>
						      			@else
						      				<button type="submit" class="btn btn-success">Reactivate</button>
						      			@endif
						      		</form>
						     	</td>
						    </tr>
						@endforeach
					</tbody>
				</table>
			@else
				@if ($errors->any())
				    <div class="alert alert-danger">
				        <ul>
				            @foreach ($errors->all() as $error)
				                <li>{{ $error }}</li>
				            @endforeach
				        </ul>
				    </div>
				@else
					{{-- check if a session flash variable containing a notificaion message is set --}}
					@if(session('status'))
						<div class="alert alert-success" role="alert">
							<h2>Thank you!</h2>
							<p>{{session('status')}}</p>
							<hr>
							<h5>Proceed to <a href="/transactions">Transactions</a>.</h5>
						</div>
					@endif
				<div class="row">
					<h1>Car Lists</h1>
				{{-- catalogue view for non-admin users --}}
					@foreach($cars as $car)

						@if($car->isActive == 1)
							<div class="media">
							  	<div style="width: 50%;" class="mx-auto">
									<img src="{{asset($car->img_path)}}" class="align-self-center mr-3" width="100%">
								</div>
							  	<div class="media-body">
									<form method="POST" action="/transactions" class="mb-3">
										@csrf
									    <h4 class="card-title mt-0" name="name">{{$car->name}}</h4>
									    <p class="card-text">{{$car->description}}</p>
										{{-- <p class="card-text">{{$asset->series->category->name}}</p> --}}
										<div class="form-control mb-2 bg-dark">
											<label for="borrow_date" class="text-white mr-3">Date of Rent:</label>
											<input type="date" id="borrow_date" name="borrow_date">										
										</div>

										<div class="form-control mb-2 bg-dark">
											<label for="return_date" class="text-white" >Date of Return:</label>
											<input type="date" id="return_date" name="return_date">									
										</div>
											<input type="hidden" name="series_id" value="{{$car->id}}">
											<button type="submit" class="btn btn-outline-success btn-lg btn-block">Rent</button>
										<hr>
									</form>
							  	</div>
							</div>
						@endif
					@endforeach
				</div>
			@endcan
		</div>
	</div>
@endsection